#include <time.h>
#include <cx16.h>
#include <cbm.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//Addresses
unsigned long tileBaseAddr1 = 0x1F000;
unsigned long mapBaseAddr1 = 0x1B000;
unsigned long tileBaseAddr0 = 0x08000;
unsigned long mapBaseAddr0 = 0x00000;
unsigned long paletteAddr = 0x1FA00;
unsigned long spriteAddr = 0x1FC00;
unsigned long psgAddr = 0x1F9C0;
unsigned long mouseGraphicAddress;

//Settings
unsigned short fieldV = 32;
unsigned short fieldH = 32;
unsigned short tilesPerMine = 10;
unsigned char zoomMode = 1;
unsigned char safeStart = 1;
unsigned char preset = 0;

unsigned char uiOffset = 2;

//Input
unsigned char joyIn1, joyIn2;
unsigned char keycode;
unsigned char mouseButtons;
unsigned short debounce = 0;
unsigned char status = 0;

//Gameplay
unsigned int tilesLeft;

clock_t start, next;

void wait() {
    start = clock();
    do {
        next = clock();
    }
    while (start == next);
    if (debounce != 0){
        debounce--;
    }
}

void waitCount(unsigned short count) {
    unsigned short i;
    for (i=0; i<count; i++) {
        wait();
    }
}

//Loads file into VRAM
void loadVera(char *fname, unsigned int address, int flag, int header){
   cbm_k_setnam(fname);
   cbm_k_setlfs(0,8,header);
   cbm_k_load(flag, address);    
}

//Returns the tile number at the given H,V positions
unsigned short getTile(unsigned short tileH, unsigned short tileV){
    VERA.address = (mapBaseAddr0 + (tileH * 2) + (tileV * 256));
    VERA.address_hi = (mapBaseAddr0 + (tileH * 2) + (tileV * 256))>>16;
    return VERA.data0;
}

//Sets the tile at position H,V with the given Value
void setTile(unsigned short tileH, unsigned short tileV, unsigned short value){
    VERA.address = (mapBaseAddr0 + (tileH * 2) + (tileV * 256));
    VERA.address_hi = (mapBaseAddr0 + (tileH * 2) + (tileV * 256))>>16;
    VERA.data0 = value;
}

//Returns the number of digits in a number
unsigned short digitCount(unsigned int value){
    unsigned int count = 0;
    while (1){
        count++;
        value /= 10;
        if (value == 0){
            break;
        }
    }
    return count;
}

//Reveals tile at position H,V
unsigned short uncover(unsigned short tileH, unsigned short tileV){
    if (getTile(tileH,tileV) == 1 || getTile(tileH,tileV) == 2){
        int h;
        int v; 
        unsigned short oldTile = getTile(tileH,tileV);
        unsigned short i = 0;
        for (h=-1;h<2;h++){
            for (v=-1;v<2;v++){
                if (!(h == -1 && tileH == 0) && !(v == -1 && tileV == 0)){
                    if (getTile(tileH+h,tileV+v) == 2 || getTile(tileH+h,tileV+v) == 6){
                        i++;
                    }
                }
            }
        }
        if (i == 0){
            setTile(tileH,tileV,3);
            tilesLeft--;
        }
        else {
            setTile(tileH,tileV,6+i);
            tilesLeft--;
        }  
        if (getTile(tileH,tileV) == oldTile){
            return 0;
        }
        else {
            return 1;
        }
    }
    else {
        return 0;
    }
}

//Displays text in layer 1 starting at position H,V with the given colors. If backcolor is 16 the colors will not be changed
void showText(char text[40], unsigned short tileH, unsigned short tileV, unsigned short backColor, unsigned short textColor){
    unsigned short i;
    VERA.address = mapBaseAddr1 + (tileH * 2) + (tileV * 256);
    VERA.address_hi = mapBaseAddr1 + (tileH * 2) + (tileV * 256)>>16;

    if (backColor == 16){
        VERA.address_hi |= 0b100000;
        for (i=0; i<strlen(text); i++){
            VERA.data0 = text[i];
        }
    }
    else {
        VERA.address_hi |= 0b10000;
        for (i=0; i<strlen(text); i++){
            VERA.data0 = text[i];
            VERA.data0 = (backColor << 4) + textColor;
        }
    }
}

//Automatically reveals spaces next to 0 tiles.
void autoReveal(unsigned short tileH, unsigned short tileV){
    unsigned short repeat = 1;
    unsigned short h;
    unsigned short v; 
    unsigned short radius = 1;

    
    showText("wait",0+uiOffset,2+uiOffset,1,7);
    while (repeat > 0 && radius < 8){
        repeat = 0;
        for (v=tileV-radius; v<=tileV+radius || v>128; v++){
            for (h=tileH-radius; h<=tileH+radius || h>128; h++){
                if (getTile(h,v) == 3){
                    repeat += uncover(h-1,v-1);
                    repeat += uncover(h-1,v+1);
                    repeat += uncover(h+1,v-1);
                    repeat += uncover(h+1,v+1);
                    repeat += uncover(h+1,v);
                    repeat += uncover(h,v-1);
                    repeat += uncover(h,v+1);
                    repeat += uncover(h-1,v);
                }
            }
        }
        radius++;
    }
    showText("    ",0+uiOffset,2+uiOffset,0,0);
}

unsigned char input(){
    status = 0;

    asm("lda #1");
    asm("jsr $FF56");
    asm("sta %v",joyIn1);
    asm("stx %v",joyIn2);

    asm("jsr $FFE4");
    asm("sta %v", keycode);

    if (debounce == 0 && joyIn1 != 255){
        status = ~joyIn1;
        debounce = 10;
    }
    if (keycode == 145 || keycode == 87){ //Up
        status |= 0b1000;
    }
    if (keycode == 17 || keycode == 83){ //Down
        status |= 0b0100;
    }
    if (keycode == 157 || keycode == 65){ //Left
        status |= 0b0010;
    }
    if (keycode == 29 || keycode == 68){ //Right
        status |= 0b0001;
    }
    return status;
}

//Main game
void game(){
    unsigned int isdone = 0;
    unsigned char h;
    unsigned char v;
    unsigned int i;
    int x;
    int y;
    unsigned char randX;
    unsigned char randY;
    char string[11];

    unsigned long spData0, spData1;

    unsigned short seconds = 0;
    unsigned short minutes = 0;
    unsigned short jiffies = 0;

    unsigned short *mouseX = (unsigned short *)0x2;
    unsigned short *mouseY = (unsigned short *)0x4;

    unsigned int mineTotal = (fieldH * fieldV) / tilesPerMine;
    unsigned int flagged = 0;

    unsigned short middleFlags;
    unsigned short middleUncovered;

    //Start Mouse
    if (zoomMode == 0){
        asm("jsr $FF5F");
        asm("lda #1");
        asm("jsr $FF68");
    }
    else {
        asm("ldx #$28");
        asm("ldy #$1E");
        asm("lda #1");
        asm("jsr $FF68");   
    }
    wait();

    _randomize();

    //Set Cursor Zdepth
    VERA.address = spriteAddr+6;
    VERA.address_hi = (spriteAddr+6)>>16;
    VERA.data0 = 0b00001000;

    //Get Mouse Graphic Address
    VERA.address = spriteAddr;
    VERA.address_hi = spriteAddr>>16;
    VERA.address_hi |= 0b10000;
    spData0 = VERA.data0;
    spData1 = VERA.data0;
    mouseGraphicAddress = ((spData1 & 0b1111)<<13) | spData0<<5;

    loadVera("sprite0.bin",mouseGraphicAddress,3,0);

    //Clear Layer 1
    VERA.address = mapBaseAddr1;
    VERA.address_hi = mapBaseAddr1>>16;
    VERA.address_hi |= 0b10000;
    for (h=0; h<128; h++){
        for (v=0; v<64; v++){
            VERA.data0 = 0;
            VERA.data0 = 0;
        }
    }

    //Generate Field
    VERA.address = mapBaseAddr0;
    VERA.address_hi = mapBaseAddr0>>16;
    VERA.address_hi |= 0b10000;
    for (v=0; v<128; v++){
        if (v < fieldV){
            for (h=0; h<128; h++){
                if (h < fieldH){
                    VERA.data0 = 1;
                    VERA.data0 = 0; 
                }
                else {
                    VERA.data0 = 0;
                    VERA.data0 = 0; 
                }
            } 
        }
        else {
            for (h=0; h<128; h++){
                VERA.data0 = 0;
                VERA.data0 = 0;
            }
        }
        
    }

    //Place Mines
    for (i=0; i<mineTotal; i++){
        isdone = 0;
        while (isdone != 1){
            h = rand() % (fieldH + 1);
            v = rand() % (fieldV + 1);

            if (getTile(h,v) == 1){
                setTile(h,v,2);
                isdone = 1;
            }
        }
    }

    //Create Static UI Elements
    showText(" time:",0+uiOffset,0+uiOffset,1,7);
    showText("mines:",0+uiOffset,1+uiOffset,1,9);

    VERA.layer0.hscroll = 0;
    VERA.layer0.vscroll = 0;

    tilesLeft = fieldH*fieldV;
    isdone = 0;
    while(isdone != 1){
        //Get Mouse Status
        asm("ldx #2");
        asm("jsr $FF6B");
        asm("sta %v", mouseButtons);

        //Left Click
        if ((mouseButtons & 0b000001) && debounce == 0){
            h = ((VERA.layer0.hscroll + *mouseX) / 16);
            v = ((VERA.layer0.vscroll + *mouseY) / 16 );

            if (tilesLeft == fieldH*fieldV && safeStart == 1){
                for (y = v-1; y <= v+1; y++){
                    for (x = h-1; x <= h+1; x++){
                        if (getTile(x,y) == 2){
                            setTile(x,y,1);

                            while (1){
                                randX = rand() % (fieldH + 1);
                                randY = rand() % (fieldV + 1);

                                if (getTile(randX,randY) == 1 && (randX < h-1 || randX > h+1) && (randY < v-1 || randY > v+1)){
                                    setTile(randX,randY,2);
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            
            if (getTile(h,v) == 1){ //Unrevealed, Safe
                uncover(h,v);
                autoReveal(h,v);
            }
            else if (getTile(h,v) == 2){ //Unrevealed, Mine
                showText("game over!",0+uiOffset,2+uiOffset,1,9);
                waitCount(60);
                for (v=0;v<fieldV;v++){
                    for (h=0;h<fieldH;h++){
                        if (getTile(h,v) == 2){ //Unrevealed, Mine
                            setTile(h,v,4);
                        }
                        if (getTile(h,v) == 6){ //Flag, Mine
                            setTile(h,v,18);
                        }
                    }
                }
                waitCount(120);
                break;
            }
            debounce = 10;
        }
        //Right Click
        else if ((mouseButtons & 0b000010) && debounce == 0){
            h = ((VERA.layer0.hscroll + *mouseX) / 16);
            v = ((VERA.layer0.vscroll + *mouseY) / 16);
            if (getTile(h,v) == 1 && flagged < mineTotal){ //Unrevealed Safe
                setTile(h,v,5);
                flagged++;
            }
            else if (getTile(h,v) == 5){ //Flag, Safe
                setTile(h,v,16);
                flagged--;
            }
            else if (getTile(h,v) == 16){ //Question, Safe
                setTile(h,v,1);
            }
            else if (getTile(h,v) == 2 && flagged < mineTotal){ //Unrevealed, Mine
                setTile(h,v,6);
                flagged++;
            }
            else if (getTile(h,v) == 6){ //Flag, Mine
                setTile(h,v,17);
                flagged--;
            }
            else if (getTile(h,v) == 17){ //Question, Mine
                setTile(h,v,2);
            }
            debounce = 10;
        }
        //Middle Click
        else if ((mouseButtons & 0b000100) && debounce == 0){
            h = ((VERA.layer0.hscroll + *mouseX) / 16);
            v = ((VERA.layer0.vscroll + *mouseY) / 16);

            middleFlags = 0;
            middleUncovered = 0;
            if (getTile(h,v) >= 7 && getTile(h,v) <= 14){
                for (x=-1;x<2;x++){
                    for (y=-1;y<2;y++){
                        if (getTile(h+x,v+y) == 1 || getTile(h+x,v+y) == 2){
                            middleUncovered++;
                        }
                        if (getTile(h+x,v+y) == 6){
                            middleFlags++;
                        }
                    }
                }

                if (getTile(h,v) - 6 == middleFlags){
                    for (x=-1;x<2;x++){
                        for (y=-1;y<2;y++){
                            uncover(h+x,v+y);
                        }
                    }
                }
                else if (getTile(h,v) - 6 == middleFlags + middleUncovered){
                    for (x=-1;x<2;x++){
                        for (y=-1;y<2;y++){
                            if (getTile(h+x,v+y) == 1 && flagged < mineTotal){ //Unrevealed Safe
                                setTile(h+x,v+y,5);
                                flagged++;
                            }
                            else if (getTile(h+x,v+y) == 2 && flagged < mineTotal){ //Unrevealed, Mine
                                setTile(h+x,v+y,6);
                                flagged++;
                            }
                        }
                    }
                } 
            }
        }
        if (mouseButtons != 0){
            if (tilesLeft <= mineTotal){
                for (v=0;v<fieldV;v++){
                    for (h=0;h<fieldH;h++){
                        if (getTile(h,v) == 3){
                            setTile(h,v,15);
                        }
                    }
                }
                waitCount(150);
                break;
            }
        }
        
        //Scrolling
        if (zoomMode == 1){
            if (*mouseX > 290){
                VERA.layer0.hscroll += 4;
            }
            else if (*mouseX < 30){
                VERA.layer0.hscroll -= 4;
            }
            if (*mouseY > 210 && zoomMode == 1){
                VERA.layer0.vscroll += 4;
            }
            else if (*mouseY < 30){
                VERA.layer0.vscroll -= 4;
            }
        }
        else {
            if (*mouseX > 610 && zoomMode == 0){
                VERA.layer0.hscroll += 8;
            }
            else if (*mouseX < 30){
                VERA.layer0.hscroll -= 8;
            }
            if (*mouseY > 450 && zoomMode == 0){
                VERA.layer0.vscroll += 8;
            }
            else if (*mouseY < 30){
                VERA.layer0.vscroll -= 8;
            }
        }

        //Time Tracker
        jiffies++;
        if (jiffies > 59){
            jiffies = 0;
            seconds++; 
            if (seconds > 59){
                seconds = 0;
                if (minutes < 99){
                    minutes++;
                }
            }
        }

        //Display Time
        if (seconds < 10){
            sprintf(string,"%d:0%d",minutes,seconds);
        }
        else {
            sprintf(string,"%d:%d",minutes,seconds);
        }
        showText(string,6+uiOffset,0+uiOffset,1,4);
        for (i=0; i<8; i++){
            VERA.data0 = 0;
            VERA.data0 = 0;
        }
   
        //Display Mines
        sprintf(string, "%d/%d", mineTotal - flagged, mineTotal);
        showText(string,6+uiOffset,1+uiOffset,1,4);
        for (i=0; i<8; i++){
            VERA.data0 = 0;
            VERA.data0 = 0;
        }

        wait();
    }
}

void main(){
    unsigned char h;
    unsigned char v;
    unsigned int i;

    char string[40];

    unsigned short isdone = 0;
    unsigned short selected = 0; 
    unsigned short lastSelected = 0;

    //Video Setup
    VERA.display.video |= 0b01110000;
	VERA.layer0.config = 0b10100010;

    //Layer 0 Addresses
    VERA.layer0.mapbase = mapBaseAddr0>>9;
    VERA.layer0.tilebase = tileBaseAddr0>>9 | 0b11;

    //Load visuals into VRAM
    loadVera("tiles.bin",tileBaseAddr0,2,0);
    loadVera("pal.bin",paletteAddr,3,0);
        
    VERA.display.video |= 0b01110000;

    VERA.address = mapBaseAddr0;
    VERA.address_hi = mapBaseAddr0>>16;
    VERA.address_hi |= 0b10000;
    for (h=0; h<128; h++){
        for (v=0; v<128; v++){
            VERA.data0 = 1;
            VERA.data0 = 0;
        }
    }

    while (1){
        //Clear Layer 1
        VERA.address = mapBaseAddr1;
        VERA.address_hi = mapBaseAddr1>>16;
        VERA.address_hi |= 0b10000;
        for (h=0; h<128; h++){
            for (v=0; v<64; v++){
                VERA.data0 = 32;
                VERA.data0 = 0;
            }
        }

        for (v=4; v<12; v++){
            showText("                  ",0+uiOffset,v+uiOffset,1,4);
        }
        showText("x16-mines",0 + uiOffset,0 + uiOffset,1,5);
        showText("by veryimpatientgamer",0 + uiOffset,1 + uiOffset,1,7);
        showText("version 1.10 dev",0 + uiOffset,2 + uiOffset,1,7);

        showText("start",0 + uiOffset,10 + uiOffset,1,4);
        showText("exit",0 + uiOffset,11 + uiOffset,1,4);
        while (isdone != 1){
            input();    

            if (status & 0b1000){ //Up
                lastSelected = selected;
                if (selected == 0){
                    selected = 7;
                }
                else {
                    selected--;
                }
            }
            else if (status & 0b0100){ //Down
                lastSelected = selected; 
                if (selected == 7){
                    selected = 0;
                }
                else {
                    selected++;
                }
            }
            else if (status & 0b0010 || status & 0b0001){ //Left or Right
                switch (selected){
                    case 0:
                    if (status & 0b0010){ //Left
                        if (preset <= 1){
                            preset = 4;
                        }
                        else {
                            preset--;
                        }
                    }
                    else { //Right
                        if (preset == 4){
                            preset = 1;
                        }
                        else {
                            preset++;
                        }
                    }
                    break;

                    case 1:
                    preset = 0;
                    if (status & 0b0010){ //Left
                        if (fieldH > 10){
                            fieldH--;
                        }
                    }
                    else { //Right
                        if (fieldH < 127){
                            fieldH++;
                        }
                    }
                    break;

                    case 2:
                    preset = 0;
                    if (status & 0b0010){ //Left
                        if (fieldV > 10){
                            fieldV--;
                        }
                    }
                    else { //Right
                        if (fieldV < 127){
                            fieldV++;
                        }
                    }
                    break;

                    case 3:
                    preset = 0;
                    if (status & 0b0010){ //Left
                        if (tilesPerMine > 2){
                            tilesPerMine--;
                        }
                    }
                    else { //Right
                        if (tilesPerMine < 25){
                            tilesPerMine++;
                        }
                    }
                    break;

                    case 4:
                    if (safeStart == 1){
                        safeStart = 0;
                    }
                    else {
                        safeStart = 1;
                    }
                    break;

                    case 5:
                    if (zoomMode == 1){
                        zoomMode = 0;
                        VERA.display.hscale = 128;
                        VERA.display.vscale = 128;
                    }
                    else{
                        zoomMode = 1;
                        VERA.display.hscale = 64;
                        VERA.display.vscale = 64;
                    }
                    break;

                    case 6:
                    isdone = 1;
                    break;

                    case 7:
                    asm("LDX #$42");
                    asm("LDY #$02");
                    asm("LDA #$00");
                    asm("JSR $FEC9");
                    break;
                }
            }

            VERA.address = mapBaseAddr1 + (4 + lastSelected + uiOffset) * 256 + 1 + (uiOffset * 2);
            VERA.address_hi = mapBaseAddr1 + (4 + lastSelected + uiOffset) * 256 + 1 + (uiOffset * 2) >> 16;
            VERA.address_hi |= 0b100000;
            for (i=0; i<18; i++){
                VERA.data0 = (1<<4) + 4;
            }

            VERA.address = mapBaseAddr1 + (4 + selected + uiOffset) * 256 + 1 + (uiOffset * 2);
            VERA.address_hi = mapBaseAddr1 + (4 + selected + uiOffset) * 256 + 1 + (uiOffset * 2) >> 16;
            VERA.address_hi |= 0b100000;
            for (i=0+uiOffset; i<18+uiOffset; i++){
                VERA.data0 = (1<<4) + 6;
            }

            sprintf(string, "field h: %d ",fieldH);
            showText(string,0 + uiOffset,5 + uiOffset,16,4);
            sprintf(string, "field v: %d ",fieldV);
            showText(string,0 + uiOffset,6 + uiOffset,16,4);
            sprintf(string, "tiles per mine: %d ",tilesPerMine);
            showText(string,0 + uiOffset,7 + uiOffset,16,4);

            //Safe Start
            if (safeStart == 1){
                showText("safe start: on ",0 + uiOffset,8 + uiOffset,1,4);
            }
            else {
                showText("safe start: off ",0 + uiOffset,8 + uiOffset,1,4);
            }

            //Zoom Mode
            if (zoomMode == 1){
                showText("zoom mode: on ",0 + uiOffset,9 + uiOffset,1,4);
                VERA.display.hscale = 64;
                VERA.display.vscale = 64;
            }
            else {
                showText("zoom mode: off",0 + uiOffset,9 + uiOffset,1,4);
                VERA.display.hscale = 128;
                VERA.display.vscale = 128;
            }
            
            //Preset
            switch (preset){
                case 0:
                showText("preset: custom ",0 + uiOffset,4 + uiOffset,1,4);
                break;

                case 1:
                showText("preset: easy   ",0 + uiOffset,4 + uiOffset,1,4);
                fieldH = 10;
                fieldV = 10;
                tilesPerMine = 10;
                break;

                case 2:
                showText("preset: medium ",0 + uiOffset,4 + uiOffset,1,4);
                fieldH = 20;
                fieldV = 15;
                tilesPerMine = 8;
                break;

                case 3:
                showText("preset: hard   ",0 + uiOffset,4 + uiOffset,1,4);
                fieldH = 35;
                fieldV = 25;
                tilesPerMine = 6;
                break;

                case 4:
                showText("preset: extreme",0 + uiOffset,4 + uiOffset,1,4);
                fieldH = 50;
                fieldV = 50;
                tilesPerMine = 4;
                break;
            }

            wait();
        }
        isdone = 0;
        game();

        VERA.address = mouseGraphicAddress;
        VERA.address_hi = mouseGraphicAddress>>16;
        VERA.address_hi |= 0b10000;
        for (i=0; i<256; i++){
            VERA.data0 = 0;
        }
    }
}