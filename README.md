**NOTICE: This version is no longer in development so I can instead focus on Version 2. You can check the current progress of Version 2 at the link below. For now though feel free to keep playing this version until Version 2 releases.**
https://codeberg.org/VeryImpatientGamer/X16-Mines-2

# X16Mines

A remake of Minesweeper for the Commander X16.

## Features:
- Up to 127x127 fields.
- Safe start mode.

## Compilation:
**Requirements:**
- [CC65 Version 2.19 or Newer.](https://cc65.github.io/)
- [X16 Emulator Release 46 or Newer.](https://github.com/X16Community/x16-emulator)

**Instructions**
1. Download the source code and extract it into a folder.
2. Go to the folder in a terminal.
3. Run the command *make*
4. Run the command *make run*

## License:
X16 Mines is subject to the CC-BY-NC-4.0 License. See the **LICENSE** file for more information.