Last Updated: 11/29/2023 (MM/DD/YYYY)

Legend:
[] = Not started
[/] = In Progress
[X] = Complete
[#] = Cancelled / Postponed

Features:
[X] Preset modes
[/] Controller / Keyboard support for gameplay
[X] Controller support for the menu
[X] Question mark tiles
[X] Tileset Updated
[X] Show correct flags on game over

Bugs / Fixes:
[] Black mouse cursor after a few rounds
[X] Move ui elements and menus away from edges to be CRT friendly
